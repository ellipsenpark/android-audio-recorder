package com.github.axet.audiorecorder.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.os.Process;
import android.util.Log;

import com.github.axet.androidlibrary.sound.AudioTrack;
import com.github.axet.audiolibrary.app.RawSamples;
import com.github.axet.audiolibrary.app.Sound;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class Monitoring {
    //public static final int PINCH = 1;
    //public static final int UPDATESAMPLES = 2;
    //public static final int END = 3;
    public static final int ERROR = 4;
    //public static final int MUTED = 5;
    //public static final int UNMUTED = 6;
    //public static final int PAUSED = 7;

    public Context context;
    public final ArrayList<Handler> handlers = new ArrayList<>();

    public Sound sound;

    public AtomicBoolean interrupt = new AtomicBoolean(); // nio throws ClosedByInterruptException if thread interrupted

    public Thread thread;

    public int bufferSize = 128;
    public int sampleRate; // variable from settings. how may samples per second.
    public RawSamples.Info info;

    public boolean isMonitoring = false;

    public Monitoring(Context context) {
        this.context = context;
        sound = new Sound(context);
        sampleRate = Sound.getSampleRate(context);
        info = new RawSamples.Info(Sound.getAudioFormat(context), sampleRate, Sound.getChannels(context));
    }

    public void toggleMonitoring() {
        if (isMonitoring) {
            stopMonitoring();
        } else {
            startMonitoring();
        }
    }

    public void startMonitoring() {
        final SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
        int source = AudioApplication.from(this.context).getSource();

        int[] ss = new int[]{
                source,
                MediaRecorder.AudioSource.MIC,
                MediaRecorder.AudioSource.DEFAULT
        };

        final AudioRecord recorder;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            recorder = new AudioRecord.Builder()
                    .setAudioSource(source)
                    .setAudioFormat(new AudioFormat.Builder()
                            .setEncoding(info.format)
                            .setSampleRate(sampleRate)
                            .setChannelMask(AudioFormat.CHANNEL_IN_STEREO)
                            .build())
                    .setBufferSizeInBytes(bufferSize * 4)
                    .build();
        } else {
            recorder = sound.createAudioRecorder(info.format, sampleRate, ss, 0);
        }

        final android.media.AudioTrack monitor;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            monitor = new AudioTrack.Builder()
                    .setAudioAttributes(new AudioAttributes.Builder()
                            .setUsage(AudioAttributes.USAGE_MEDIA)
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .build())
                    .setAudioFormat(new AudioFormat.Builder()
                            .setEncoding(info.format)
                            .setSampleRate(sampleRate)
                            .setChannelMask(AudioFormat.CHANNEL_OUT_STEREO)
                            .build())
                    .setPerformanceMode(android.media.AudioTrack.PERFORMANCE_MODE_LOW_LATENCY)
                    .setBufferSizeInBytes(bufferSize * 4)
                    .build();
        } else {
            AudioTrack.AudioBuffer buf = new AudioTrack.AudioBuffer(sampleRate, Sound.getOutMode(this.context), info.format, bufferSize);
            monitor = AudioTrack.create(Sound.SOUND_STREAM, Sound.SOUND_CHANNEL, Sound.SOUND_TYPE, buf);
        }

        final Thread old = thread;
        final AtomicBoolean oldb = interrupt;

        interrupt = new AtomicBoolean(false);
        thread = new Thread("RecordingThread") {
            @Override
            public void run() {
                if (old != null) {
                    oldb.set(true);
                    old.interrupt();
                    try {
                        old.join();
                    } catch (InterruptedException e) {
                        return;
                    }
                }

                Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO);

                try {
                    recorder.startRecording();
                    monitor.play();

                    AudioTrack.SamplesBuffer buffer = new AudioTrack.SamplesBuffer(info.format, bufferSize);
                    float amp = shared.getFloat(AudioApplication.PREFERENCE_MONITOR_VOLUME, 0.75f);

                    while (!interrupt.get()) {
                        int readSize = -1;
                        switch (buffer.format) {
                            case AudioFormat.ENCODING_PCM_8BIT:
                                break;
                            case AudioFormat.ENCODING_PCM_16BIT:
                                readSize = recorder.read(buffer.shorts, 0, buffer.shorts.length);
                                monitor.write(buffer.shorts, 0, readSize);
                                break;
                            case Sound.ENCODING_PCM_24BIT_PACKED:
                                break;
                            case Sound.ENCODING_PCM_32BIT:
                                break;
                            case AudioFormat.ENCODING_PCM_FLOAT:
                                if (Build.VERSION.SDK_INT >= 23) {
                                    readSize = recorder.read(buffer.floats, 0, buffer.floats.length, AudioRecord.READ_BLOCKING);
                                    for (int i = 0; i < buffer.floats.length; i++) {
                                        buffer.floats[i] *= amp;
                                    }
                                    monitor.write(buffer.floats, 0, readSize, AudioTrack.WRITE_BLOCKING);
                                }
                                break;
                            default:
                                throw new RuntimeException("Unknown format");
                        }
                    }
                } catch (final RuntimeException e) {
                    Log.e("MON", "error while monitoring", e);
                    Post(e);
                } finally {
                    if (recorder != null)
                        recorder.release();

                    if (monitor != null) {
                        monitor.stop();
                        monitor.release();
                    }
                }
            }
        };
        isMonitoring = true;
        thread.start();
    }

    public void stopMonitoring() {
        if (thread != null) {
            interrupt.set(true);
            try {
                thread.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            thread = null;
        }
        sound.unsilent();
        isMonitoring = false;
    }

    public void Post(Throwable e) {
        Post(ERROR, e);
    }

    public void Post(int what, Object p) {
        synchronized (handlers) {
            for (Handler h : handlers)
                h.obtainMessage(what, p).sendToTarget();
        }
    }
}
